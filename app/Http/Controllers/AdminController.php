<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin-web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin');
    }

    public function show()
    {
        $user = Admin::find(Auth::user()->id);
        return view('auth.admin.admin-user')->with('user', $user);
    }

    public function update(Request $request)
    {
        // Get current user
        $userId =Auth::user()->id;
        $admin = Admin::findOrFail($userId);

        // Validate the data submitted by user
        //dd($request);
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:225|'. Rule::unique('admins')->ignore($admin->id),
            'city' => 'required|string|max:255',
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
            'country' => 'required|string|max:255',
            'age' => 'required|integer',
        ]);

        // if fails redirects back with errors
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        // Image Updation
        if($file = $request->hasFile('image')) {

            $file = $request->file('image') ;
            $fileName = $request->name."-image-".time().".".$file->extension();        
            $destinationPath = public_path().'/user_profile/' ;
            $filePath = '/user_profile/'.$fileName;
            $file->move($destinationPath,$fileName);

            if($admin->image != 'http://blog.localhost.com/assets/default.png'){
                $url = $admin->image;
                $path = substr(parse_url($url, PHP_URL_PATH), 1);
                if(file_exists(public_path("$path"))){
                    unlink(public_path("$path"));
                }
            }

        }else{
            $filePath = $admin->image;
        }

        $admin->fill([
            'name' => $request->name,
            'email' => $request->email,
            'city' => $request->city,
            'country' => $request->country,
            'image' => url("$filePath"),
            'age' => $request->age,
        ]);
            

        // Save user to database
        $admin->save();

        // Redirect to route
        return redirect()->route('admin.user');
    }
}
