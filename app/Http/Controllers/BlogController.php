<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;

class BlogController extends Controller
{

    public function getIndex(){
        $posts = Post::orderBy('created_at', 'desc')->where('block', false)->paginate(5);

        return view('blog.index')->with('posts', $posts);
    }


    public function getSingle($slug){
        $post = Post::where('slug', '=', $slug)->first();

        return view('blog.single')->with('post', $post);
    }

    public function searchPost(Request $request){
        $date_query = null;

        $title = $request->title;
        $city = $request->city;
        $country = $request->country;
        $user = $request->user;

        if($request->from != null && $request->to != null){
            $from = date('Y-m-d H:i:s', strtotime($request->from));
            $to = date('Y-m-d H:i:s', strtotime($request->to)+86399);
            $date_query = [$from, $to];
        }elseif($request->from != null && $request->to == null){
            $from = date('Y-m-d H:i:s', strtotime($request->from));
            $now        = date('Y-m-d H:i:s', time());
            $date_query = [$from, $now];
        }elseif($request->from == null && $request->to != null){
            $to = date('Y-m-d H:i:s', strtotime($request->to)+86399);
            $date_query = [0, $to];
        }

        $users = User::
        when($city, function ($query, $city) {
            return $query->where('city', 'like', "%$city%");
        })->
        when($country, function ($query, $country) {
            return $query->where('country', 'like', "%$country%");
        })->
        when($user, function ($query, $user) {
            return $query->where('name', 'like', "%$user%");
        })
        ->get();

        $users_id = array();
        foreach($users as $user){
            array_push($users_id, $user->id);
        }
        
        $posts = Post::
        when($title, function ($query, $title) {
            return $query->where('title', 'like', "%$title%");
        })->
        when($date_query, function ($query, $date_query) {
            return $query->whereBetween('created_at', $date_query);
        })->
        when($users_id, function ($query, $users_id) {
            return $query->whereIn('user_id', $users_id);
        })
        ->paginate(10);
        return view('blog.index')->with('posts',$posts);
    }
}
