<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Post;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminPostController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin-web');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('id', 'desc')->paginate(5);
        return view('admin.index')->with('posts',$posts);
    }

    public function indexBlock()
    {
        $posts = Post::orderBy('id', 'desc')->where('block', true)->paginate(5);
        return view('admin.index')->with('posts',$posts);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        return view('admin.show')->with('post', $post);
    }

    public function showUser($id)
    {
        $post = Post::find($id);

        return view('admin.show-user-post')->with('post', $post);
    }

    public function searchPost(Request $request){
        $date_query = null;

        $title = $request->title;
        $city = $request->city;
        $country = $request->country;
        $user = $request->user;

        if($request->from != null && $request->to != null){
            $from = date('Y-m-d H:i:s', strtotime($request->from));
            $to = date('Y-m-d H:i:s', strtotime($request->to)+86399);
            $date_query = [$from, $to];
        }elseif($request->from != null && $request->to == null){
            $from = date('Y-m-d H:i:s', strtotime($request->from));
            $now        = date('Y-m-d H:i:s', time());
            $date_query = [$from, $now];
        }elseif($request->from == null && $request->to != null){
            $to = date('Y-m-d H:i:s', strtotime($request->to)+86399);
            $date_query = [0, $to];
        }

        $users = User::
        when($city, function ($query, $city) {
            return $query->where('city', 'like', "%$city%");
        })->
        when($country, function ($query, $country) {
            return $query->where('country', 'like', "%$country%");
        })->
        when($user, function ($query, $user) {
            return $query->where('name', 'like', "%$user%");
        })
        ->get();

        $users_id = array();
        foreach($users as $user){
            array_push($users_id, $user->id);
        }
        
        $posts = Post::
        when($title, function ($query, $title) {
            return $query->where('title', 'like', "%$title%");
        })->
        when($date_query, function ($query, $date_query) {
            return $query->whereBetween('created_at', $date_query);
        })->
        when($users_id, function ($query, $users_id) {
            return $query->whereIn('user_id', $users_id);
        })
        ->paginate(10);
        return view('admin.index')->with('posts',$posts);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $post = Post::find($id);

        if($post->block){
            $post->block = false;
            Session::flash('success', 'Blog Successfully UNBLOCK.');
        }else{
            $post->block = true;
            Session::flash('success', 'Blog Successfully UNBLOCK.');
        }
        $post->save();

        return redirect()->route('admin.posts.show', $post->id);
    }

    public function updateUser($id)
    {
        $post = Post::find($id);

        if($post->block){
            $post->block = false;
            Session::flash('success', 'Blog Successfully UNBLOCK.');
        }else{
            $post->block = true;
            Session::flash('success', 'Blog Successfully BLOCK.');
        }
        $post->save();

        return redirect()->route('admin.users.show', $post->user_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        foreach ($post->comments as $comment)
        {
            $comment->delete();
        }
        
        $post->delete();

        Session::flash('success', 'Successfully delete the Blog.');
        return redirect()->route('admin.index');
    }

    public function destroyUser($id)
    {
        $post = Post::find($id);

        $user_id = $post->user_id;

        if($post->comments->count() > 0){
            foreach ($post->comments as $comment)
            {
                $comment->delete();
            }
        }
        
        $post->delete();

        Session::flash('success', 'Successfully delete the Blog.');
        return redirect()->route('admin.users.show', $user_id);
    }
}
