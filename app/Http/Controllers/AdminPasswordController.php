<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AdminPasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin-web');
    }

    public function show()
    {
        return view('auth.admin.admin-password-update');
    }

    public function update(Request $request)
    {
        // Get current user
        $userId = Auth::id();
        $admin = Admin::findOrFail($userId);

        // Validate the data submitted by user
        //dd($request);
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed',
            'old_password' => 'required|string|min:6',
        ]);

        // if fails redirects back with errors
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }


        if(Hash::check($request->old_password, $admin->password))
        {
            $new_pass = Hash::make($request->password);
            $admin->fill([
                'password' => $new_pass,
            ]);
            $admin->save();
            return redirect()->route('admin.user');
        }

        Session::flash('fail', 'Enter Correct Old Password');
        return redirect()->route('admin.user');
        
    }
}
