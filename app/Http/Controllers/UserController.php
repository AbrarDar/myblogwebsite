<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        $user = User::find(Auth::user()->id);
        return view('auth.user')->with('user', $user);
    }

    /**
     * @param Request $request
     * @return
     */
    public function update(Request $request)
    {
        // Get current user
        $userId = Auth::id();
        $user = User::findOrFail($userId);

        // Validate the data submitted by user
        //dd($request);
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:225|'. Rule::unique('users')->ignore($user->id),
            'city' => 'required|string|max:255',
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
            'country' => 'required|string|max:255',
            'age' => 'required|integer',
            'password' => 'string|min:6',
        ]);

        // if fails redirects back with errors
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        // Image Updation
        if($file = $request->hasFile('image'))
        {
            $file = $request->file('image') ;
            $fileName = $request->name."-image-".time().".".$file->extension();        
            $destinationPath = public_path().'/user_profile/' ;
            $filePath = '/user_profile/'.$fileName;
            $file->move($destinationPath,$fileName);

            if($user->image != 'http://blog.localhost.com/assets/default.png')
            {
                $url = $user->image;
                $path = substr(parse_url($url, PHP_URL_PATH), 1);
                if(file_exists(public_path("$path"))){
                    unlink(public_path("$path"));
                }
            }

        }else{
            $filePath = $user->image;
        }

        $new_pass = $user->password;
        if(isset($request->password)){
            if(isset($request->old_password)){
                if(Hash::check($request->old_password, $user->password))
                {
                    $new_pass = Hash::make($request->password);
                }else{
                    Session::flash('fail', 'Enter Correct Old Password');
                    return redirect()->route('user');
                }
            }else{
                Session::flash('fail', 'Enter Old Password');
                return redirect()->route('user');
            }
            
        }

        $user->fill([
            'name' => $request->name,
            'email' => $request->email,
            'city' => $request->city,
            'country' => $request->country,
            'image' => url("$filePath"),
            'password' => $new_pass,
            'age' => $request->age,
        ]);
        

        // Save user to database
        $user->save();

        // Redirect to route
        return redirect()->route('user');
    }
}