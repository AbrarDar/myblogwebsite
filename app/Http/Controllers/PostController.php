<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\Comment;
use Auth;
use Illuminate\Support\Facades\Session;


class PostController extends Controller
{
    public function __construct(){
        $this->middleware('auth:web');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('id', 'desc')->where([
            ['user_id', Auth::user()->id]
        ])->paginate(2);
        if(Auth::user()->block){
            Session::flash('blocked', 'Your are Blocked. Your post will not show to other yours.');
        }
        return view('posts.index')->with('posts',$posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->block){
            Session::flash('blocked', 'Your are Blocked. Your post will not show to other yours.');
        }
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'title' => 'required|max:255',
            'body' => 'required'
        ));

        

        $post = new Post;
        $post->title = $request->title;
        $post->body = $request->body;
        $post->slug = str_replace(' ', '-', substr($request->title, 0, 200)).time();
        $post->user_id = Auth::user()->id;
        $post->block = Auth::user()->block;

        $post->save();

        Session::flash('success', 'Successfully created the Blog.');
        if(Auth::user()->block){
            Session::flash('blocked', 'Your are Blocked. Your post will not show to other yours.');
        }
        return redirect()->route('posts.show', $post->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'title' => 'required|max:255',
            'body' => 'required'
        ));

        $post = Post::find($id);

        $post->title = $request->input('title');
        $post->body = $request->input('body');

        $post->save();

        Session::flash('success', 'Successfully saved the Blog.');
        return redirect()->route('posts.show', $post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        foreach ($post->comments as $comment)
        {
            $comment->delete();
        }
        
        $post->delete();

        Session::flash('success', 'Successfully delete the Blog.');
        return redirect()->route('posts.index');
    }
}
