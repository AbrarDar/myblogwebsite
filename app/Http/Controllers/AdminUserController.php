<?php

namespace App\Http\Controllers;


use Auth;
use Mail;
use App\User;
use App\Post;
use App\Comment;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class AdminUserController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin-web');
    }

    public function index()
    {
        $users = User::orderBy('id', 'desc')->paginate(2);
        return view('admin.users')->with('users',$users);
    }

    public function indexBlock()
    {
        $users = User::orderBy('id', 'desc')->where('block', true)->paginate(2);
        return view('admin.users')->with('users',$users);
    }

    public function show($id)
    {
        $user = User::find($id);
        return view('admin.users-show')->with('user', $user);
    }

    public function update($id)
    {
        $user = User::find($id);

        if($user->block){
            foreach ($user->posts as $post)
            {
                $post->block = false;
                $post->save();
            }
            $user->block = false;
            Session::flash('success', 'User Successfully UNBLOCK.');
        }else{
            foreach ($user->posts as $post)
            {
                $post->block = true;
                $post->save();
            }
            $user->block = true;
            Session::flash('success', 'User Successfully UNBLOCK.');
        }
        $user->save();

        $data = array(
            'email' => $user->email,
            'name' => $user->name,
            'block' => $user->block,
        );

        Mail::send('mail.mail-blocked', $data, function($message) use($data){
            $message->from('admin@blog.localhost.com');
            $message->to($data['email']);
            $message->subject( $data['email'] ? "Account Blocked" : "Account Unblocked");
        });


        if($user->block){
            Session::flash('blocked', "User Blocked");
        }else{
            Session::flash('unblocked', "User Unblocked");
        }
        return redirect()->route('admin.users.index');
    }

    public function destroy($id)
    {
        $user = User::find($id);

        if($user->posts->count() > 0){
            foreach ($user->posts as $post)
            {
                if($post->comments->count() > 0){
                    foreach ($post->comments as $comment)
                    {
                        $comment->delete();
                    }
                }
                
                $post->delete();
            }
        }
        
        $user->delete();

        Session::flash('success', 'Successfully delete the User.');
        return redirect()->route('admin.users.index');
    }

    public function searchUser(Request $request){
        $date_query = null;

        
        $user = $request->name;
        $gender = $request->gender;
        $email = $request->email;
        $city = $request->city;
        $country = $request->country;

        $users = User::
        when($city, function ($query, $city) {
            return $query->where('city', 'like', "%$city%");
        })->
        when($country, function ($query, $country) {
            return $query->where('country', 'like', "%$country%");
        })->
        when($user, function ($query, $user) {
            return $query->where('name', 'like', "%$user%");
        })->
        when($gender, function ($query, $gender) {
            return $query->where('gender', 'like', "%$gender%");
        })->
        when($email, function ($query, $email) {
            return $query->where('email', 'like', "%$email%");
        })
        ->paginate(5);

        return view('admin.users')->with('users',$users);
    }
}
