<?php
namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use App\Post;

class PagesController extends Controller{

    public function getIndex(){
        $posts = Post::orderBy('created_at', 'desc')->where('block', false)->limit(5)->get();
        return view('pages.welcome')->with('posts', $posts);
    }

    public function getAbout(){
        return view('pages.about');
    }
}