<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function posts(){
        return $this->belongsToMany('App\Post');
    }

    public function users(){
        return $this->belongsToMany('App\User');
    }
}
