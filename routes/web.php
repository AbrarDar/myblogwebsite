<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::post('blog/search', 'BlogController@searchPost')->name('admin.blog.search');
Route::get('/blog/{slug}', 'BlogController@getSingle')->name('blog.single');

Route::get('/blog', 'BlogController@getIndex')->name('blog.index');

Route::get('/about', 'PagesController@getAbout');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user/logout', 'Auth\LoginController@userLogout')->name('user.logout');

Route::get('/admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
Route::get('/admin/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

Route::get('/admin/register', 'Auth\AdminRegisterController@showRegistrationForm')->name('admin.register');
Route::post('/admin/register', 'Auth\AdminRegisterController@register')->name('admin.register.submit');

Route::post('/user/password/update', 'PasswordController@update')->name('user.password.update');
Route::get('/user/password', 'PasswordController@show')->name('user.password');
Route::post('/user/update', 'UserController@update')->name('user.update');
Route::get('/user', 'UserController@show')->name('user');

Route::post('/admin/password/update', 'AdminPasswordController@update')->name('admin.password.update');
Route::get('/admin/password', 'AdminPasswordController@show')->name('admin.password');


Route::post('/admin/posts/search', 'AdminPostController@searchPost')->name('admin.posts.search');
Route::get('/admin/posts/block', 'AdminPostController@indexBlock')->name('admin.posts.index.block');
Route::get('/admin/posts', 'AdminPostController@index')->name('admin.posts.index');
Route::get('/admin/posts/{post}', 'AdminPostController@show')->name('admin.posts.show');
Route::put('/admin/posts/{post}', 'AdminPostController@update')->name('admin.posts.update');
Route::delete('/admin/posts/{post}', 'AdminPostController@destroy')->name('admin.posts.destroy');


Route::get('/admin/users/posts/{post}', 'AdminPostController@showUser')->name('admin.user.posts.show');
Route::put('/admin/users/posts/{post}', 'AdminPostController@updateUser')->name('admin.user.posts.update');
Route::delete('/admin/users/posts/{post}', 'AdminPostController@destroyUser')->name('admin.user.posts.destroy');

Route::post('/admin/users/search', 'AdminUserController@searchUser')->name('admin.users.search');
Route::get('/admin/users/block', 'AdminUserController@indexBlock')->name('admin.users.index.block');
Route::get('/admin/users', 'AdminUserController@index')->name('admin.users.index');
Route::get('/admin/users/{user}', 'AdminUserController@show')->name('admin.users.show');
Route::put('/admin/users/{user}', 'AdminUserController@update')->name('admin.users.update');
Route::delete('/admin/users/{user}', 'AdminUserController@destroy')->name('admin.users.destroy');


Route::get('/admin/user', 'AdminController@show')->name('admin.user');
Route::post('/admin/update', 'AdminController@update')->name('admin.user.update');

Route::get('/admin', 'AdminController@index')->name('admin');

Route::get('/', 'PagesController@getIndex');

Route::resource('posts', 'PostController');
Route::resource('comments', 'CommentController');