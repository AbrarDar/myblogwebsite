@extends('layouts.app')

@section('title', ' | View Post')

@section('content')

@if (Session::has('success'))
    <div class="row">
        <div class="col-md-10 offset-md-1 alert alert-success alert-dismissible fade show" role="alert">
            <p>{{ Session::get('success') }}</p>
        </div>
    </div>
@endif
@if (Session::has('blocked'))
    <div class="row">
        <div class="col-md-10 offset-md-1 alert alert-danger alert-dismissible fade show" role="alert">
            <p>{{ Session::get('blocked') }}</p>
        </div>
    </div>
@endif

<div class="row">
    <div class="col-sm-7 offset-sm-1">
        <h1>{{ $post->title }}</h1>
        <p class="lead"> {{ $post->body }}</p>
    </div>

    <div class="col-sm-2 offset-sm-1">
        <div class="well">
            <dl class="dl-horizontal">
                <dt>URL:</dt>
                <dd><a href="{{ route('blog.single',$post->slug) }}">{{substr(url('blog/'.$post->slug), 0 ,20)}}{{ strlen(url($post->slug))>20 ? "..." : ""}}</a></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Create At:</dt>
                <dd>{{date('M j, Y h:iA', strtotime($post->created_at))}}</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Last Update At:</dt>
                <dd>{{date('M j, Y h:iA', strtotime($post->updated_at))}}</dd>
            </dl>
            <div class="row">
                <div class="col-sm-6">
                    {!! Html::linkRoute('posts.edit', 'Edit', array($post->id), array('class'=>'btn btn-primary btn-block')) !!}
                    
                </div>
                <div class="col-sm-6">
                    {!! Form::open(['route'=>['posts.destroy', $post->id], 'method' => 'DELETE']) !!}
                    {!! Form::submit('Delete', ['class'=>'btn btn-danger btn-block']) !!}
                    {!! Form::close() !!}
                </div>
                
            </div>
            <div class="row">
                <div class="col-lg-12">
                    {{ Html::linkRoute('posts.index', '<< See All Posts', [], ['class' => 'btn btn-primary btn-block', 'style'=>'margin-top: 10px; align-items:center;'])}}
                </div>
            </div>
        </div>
    </div>
</div>



    
@endsection