@extends('layouts.app')

@section('title', ' | Edit Post')

@section('content')

<div class="row">
    <div class="col-md-12" style="">
    {!! Form::model($post, ['route'=> ['posts.update', $post->id], 'method' => 'PUT', 'style'=>'width: 100%; display: flex;']) !!}
    <div class="col-md-6 offset-md-1">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class'=>'form-control input-lg']) !!}
        {!! Form::label('body', 'Blog:', ['class'=>'top-spacing']) !!}
        {!! Form::textarea('body', null, ['class'=>'form-control']) !!}
    </div>

    <div class="col-md-3 offset-md-1">
        <div class="well top-spacing">
            <dl class="dl-horizontal">
                <dt>URL:</dt>
                <dd><a href="{{ route('blog.single', $post->slug) }}">{{substr(url('blog/'.$post->slug), 0 ,20)}}{{ strlen(url($post->slug))>20 ? "..." : ""}}</a></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Create At:</dt>
                <dd>{{date('M j, Y h:iA', strtotime($post->created_at))}}</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Last Update At:</dt>
                <dd>{{date('M j, Y h:iA', strtotime($post->updated_at))}}</dd>
            </dl>
            <div class="row">
                <div class="col-sm-6">
                    {!! Form::submit('Save', ['class'=>'btn btn-primary btn-block']) !!}
                </div>
                <div class="col-sm-6">
                    
                    {!! Html::linkRoute('posts.show', 'Cancel', array($post->id), array('class'=>'btn btn-danger btn-block')) !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
</div>



    
@endsection