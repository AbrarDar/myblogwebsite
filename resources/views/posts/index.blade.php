@extends('layouts.app')

@section('title', ' | All Post')

@section('content')

    @if (Session::has('success'))
    <div class="row">
        <div class="col-md-10 offset-md-1 alert alert-success alert-dismissible fade show" role="alert">
            <p>{{ Session::get('success') }}</p>
        </div>
    </div>
    @endif
    @if (Session::has('blocked'))
    <div class="row">
        <div class="col-md-10 offset-md-1 alert alert-danger alert-dismissible fade show" role="alert">
            <p>{{ Session::get('blocked') }}</p>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-md-8 offset-md-1">
            <h1>All Post</h1>
        </div>
        <div class="col-md-2">
            <a href="{{ route('posts.create') }}" class="btn btn-lg btn-block btn-primary">Create New Post</a>
        </div>
        <div class="col-md-10 offset-md-1">
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <table class="table" style="margin-top: 20px;">
                <thead>
                    <th style="width: 35%">Title</th>
                    <th style="width: 45%">Body</th>
                    <th style="width: 15%">Create</th>
                    <th style="width: 5%"></th>
                </thead>
                <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <td>{{ $post->title }}</td>
                            <td>{{ substr($post->body, 0, 250) }}{{ strlen($post->body)>250 ? "..." : ""}}</td>
                            <td>{{ date('M j, Y h:iA', strtotime($post->created_at)) }}</td>
                            <td>
                                <div class="row">
                                    <a href="{{ route('posts.show', $post->id)}}" class="btn btn-primary  btn-block">View</a>
                                    <a href="{{ route('posts.edit', $post->id)}}" class="btn btn-primary btn-block">Edit</a>
                                    {!! Form::open(['route'=>['posts.destroy', $post->id], 'method' => 'DELETE']) !!}
                                    {!! Form::submit('Delete', ['class'=>'btn btn-primary btn-block', 'style'=>'margin-top: 10px;']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                
            </table>
            <hr>

            <div class="" style="display: flex; justify-content: center;">
                {!! $posts->links(); !!}
            </div>
        </div>
    </div>
@endsection