@extends('layouts.app')

@section('title', ' | Create Post')

@section('content')
    @if (Session::has('blocked'))
    <div class="row">
        <div class="col-md-10 offset-md-1 alert alert-danger alert-dismissible fade show" role="alert">
            <p>{{ Session::get('blocked') }}</p>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <h1>Create New Post</h1>
            <hr>
            {!! Form::open(['route' => 'posts.store']) !!}
                {!! Form::label('title', 'Title:') !!}
                {!! Form::text('title', null, array('class' => 'form-control')) !!}

                {!! Form::label('body', 'Post:') !!}
                {!! Form::textarea('body', null, array('class' => 'form-control')) !!}

                {!! Form::submit('Create Post', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin: 20px 0 20px 0;')) !!}
            {!! Form::close() !!}

        </div>
    </div>
    
@endsection