@extends('layouts.app')

@section('title', " | $post->title")

@section('content')

<div class="row">
    <div class="col-md-8 offset-md-2">
        <h1>{{$post->title}}</h1>
        <p>{{$post->body}}</p>
    </div>
</div>

@foreach ($post->comments as $comment)
<div class="row">
    <div class="col-md-8 offset-md-2">
        <div class="row" >
            @foreach ($comment->users as $user)
            <div class='col-md-3'>
                <div class='row'>
                    <img  src="{{URL::to("$user->image")}}" alt="User Image" style='width: 30px;height: 30px; border-radius: 50%; margin-left: 25px; margin-right: 10px;
                    object-fit: cover;'>
                    <h5  style="text-align: Right;">{{$user->name}}:</h5>
                </div>
            </div>
            @endforeach
        
        <p class="col-md-9" style="background: white;">{{$comment->comment}}</p>
        </div>
    </div>
</div>
@endforeach


<div class="row">
    <div class="col-md-8 offset-md-2">
        {!! Form::open(['route' => 'comments.store']) !!}
        {!! Form::hidden('post_id', $post->id, array('class' => 'form-control')) !!}
        {!! Form::text('comment', 'comment here', array('class' => 'form-control')) !!}
        <div class="col-md-2 offset-md-5">
        {!! Form::submit('Post', array('class' => 'btn btn-success btn-lg align-content-center', 'style' => 'margin: 20px 0 20px 0;')) !!}
        </div>
        {!! Form::close() !!}
    </div>
</div>



@endsection