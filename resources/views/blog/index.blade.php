@extends('layouts.app')

@section('title', " | Page Blogs")

@section('content')

    <div class="row">
        <div class="col-md-6 offset-md-2">
            <h1>Blog</h1>
        </div>
        <div class="col-md-2">
            <button class="searchBlog btn btn-lg btn-block btn-primary" >Search Post</button>
        </div>
    </div>

    <div class="row" style="margin:10px;">
        <div class="col-md-8 offset-md-2">
            <div class="searchForm" style="display: none;">
                <div class="card">
                    
                    <div class="card-header text-center">
                        <h3>{{ __('Post Search') }}</h3>
                    </div>
    
                    <div class="card-body">
    
                        <form method="POST" action="{{ route('admin.blog.search') }}" aria-label="{{ __('Register') }}">
                            @csrf
    
                            <div class="form-group row">
                                <label for="title" class="col-md-2 col-form-label text-md-right">{{ __('Title') }}</label>
                                <div class="col-md-8">
                                    <input id="title" type="text"
                                        class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title"
                                        value="{{ old('title') }}" >
    
                                    @if ($errors->has('title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="user"
                                    class="col-md-2 col-form-label text-md-right">{{ __('Auhtor') }}</label>
                                <div class="col-md-8">
                                    <input id="user" type="text"
                                        class="form-control{{ $errors->has('user') ? ' is-invalid' : '' }}" name="user"
                                        value="{{ old('user') }}" >
    
                                    @if ($errors->has('user'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('user') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="city" class="col-md-2 col-form-label text-md-right">{{ __('City') }}</label>
                                <div class="col-md-8">
                                    <input id="city" type="text"
                                        class="form-control" name="city"
                                        value="{{ old('city') }}" >
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="country"
                                    class="col-md-2 col-form-label text-md-right">{{ __('Country') }}</label>    
                                <div class="col-md-8">
                                    <input id="country" type="text"
                                        class="form-control"
                                        name="country" value="{{ old('country') }}" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="from" class="col-md-2 col-form-label text-md-right">{{ __('From') }}</label>
                                <div class="col-md-8">
                                    <input id="from" type="date"
                                        class="form-control{{ $errors->has('from') ? ' is-invalid' : '' }}" name="from"
                                        value="{{ old('from') }}" >
    
                                    @if ($errors->has('from'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('from') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="to" class="col-md-2 col-form-label text-md-right">{{ __('To') }}</label>    
                                <div class="col-md-8">
                                    <input id="to" type="date"
                                        class="form-control{{ $errors->has('to') ? ' is-invalid' : '' }}" name="to"
                                        value="{{ old('to') }}" >
    
                                    @if ($errors->has('to'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('to') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-2 offset-md-5">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        {{ __('Search') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <hr>
            <br>
        </div>
    </div>

    @foreach ($posts as $post)
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <h2>{{$post->title}}</h2>
            <h5>Published: {{date('M j, Y h:iA', strtotime($post->created_at))}}</h5>
            <p>
                {{ substr($post->body, 0, 250) }}{{strlen($post->body)>250 ? "..." : ""}}
            </p>
            <a href="{{ url('blog/'.$post->slug)}}" class="btn btn-primary">Read More..</a>
            <hr>
        </div>
    </div>
    @endforeach

    <div class="" style="display: flex; justify-content: center;">
        {!! $posts->links() !!}
    </div>

@endsection