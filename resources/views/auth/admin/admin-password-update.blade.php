@extends('layouts.app')

@section('content')
    <div class="container">

        @if (Session::has('fail'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <h4 class="alert-heading">Failed!</h4>
                <p>{{ Session::get('fail') }}</p>
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" style="text-align: center">
                        <h4>New Password</h4>
                    </div>
                    
                    <div class="card-body">

                        @if($errors)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger" role="alert">
                                    {{ $error }}
                                </div>
                            @endforeach
                        @endif

                        {!! Form::open( ['route' => ['admin.password.update'], 'method' => 'POST']) !!}

                            <div class="form-group row">
                                <label for="password" class="col-sm-3 col-form-label">New Password</label>
                                <div class="col-sm-9">
                                    {!! Form::password('password', ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password_confirmation" class="col-sm-3 col-form-label">Confirm Password</label>
                                <div class="col-sm-9">
                                    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="old_password" class="col-sm-3 col-form-label">Old Password</label>
                                <div class="col-sm-9">
                                    {!! Form::password('old_password',  ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            
                            <div class="form-group col-md-7 offset-md-5 justify-content-center">
                                {!! Form::submit('Update Password', ['class' => 'btn btn-primary ']) !!}
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection