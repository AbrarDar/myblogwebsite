@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <div class="jumbotron">
                <h1>Welcome to My Blog</h1>
                <p class="lead">This my first blog website</p>
                <p><a class="btn btn-primary btn-lg" href="/blog" role="button">Latest Posts</a></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-md-1">
            @foreach ($posts as $post)
            <div class="post">
                <h1>{{$post->title}}</h1>
                <p>
                    {{ substr($post->body, 0, 200) }}{{strlen($post->body)>200 ? "..." : ""}}
                </p>
                <a href="{{ url('blog/'.$post->slug)}}" class="btn btn-primary">Read More..</a>
                <hr>
            </div>
            @endforeach
        </div>
    </div>
@endsection