@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <h1>About me</h1>
            <p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente, fuga dignissimos
                perferendis impedit animi, mollitia inventore dolor atque eos fugit omnis similique autem, expedita
                maxime eius vero nisi culpa amet.</p>
        </div>
    </div>
@endsection