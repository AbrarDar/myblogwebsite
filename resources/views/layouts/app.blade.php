<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ Auth::guard('admin-web')->check() ? url('/admin') : url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    
                    @if (Auth::guard('admin-web')->check())
                        <ul class="navbar-nav mr-auto">
                            <li class="{{ Request::is('/admin/posts')? "active": ""}} "><a class="navbar-brand" href="/admin/posts">Posts</a></li>
                            <li class="{{ Request::is('/admin/users')? "active": ""}} "><a class="navbar-brand" href="/admin/users">Users</a></li>
                            <li class="{{ Request::is('/admin/user')? "active": ""}} "><a class="navbar-brand" href="/admin/user">Profile</a></li>
                        </ul>
                    @else
                        <ul class="navbar-nav mr-auto">
                            <li class="{{ Request::is('/')? "active": ""}} "><a class="navbar-brand" href="/">Home</a></li>
                            <li class="{{ Request::is('/blog')? "active": ""}} "><a class="navbar-brand" href="/blog">Blogs</a></li>
                            <li class="{{ Request::is('/posts/create')? "active": ""}} "><a class="navbar-brand" href="/posts">Posts</a></li>
                            <li class="{{ Request::is('/user')? "active": ""}} "><a class="navbar-brand" href="/user">Profile</a></li>
                            <li class="{{ Request::is('/about')? "active": ""}} "><a class="navbar-brand" href="/about">About</a></li>
                        </ul>
                    @endif

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @if (Auth::guard('web')->check())
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="/" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    
                                    <a class="dropdown-item" href="{{ route('posts.index') }}">
                                        {{ __('My Posts') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('posts.create') }}">
                                        {{ __('Create New Post') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('user') }}">
                                        {{ __('Profile') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('user.password') }}">
                                        {{ __('Change Password') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('user.logout') }}"
                                        onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('user.logout') }}" method="GET" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @elseif (Auth::guard('admin-web')->check() )
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="/" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    
                                    <a class="dropdown-item" href="{{ route('admin.posts.index') }}">
                                        {{ __('All Posts') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('admin.user') }}">
                                        {{ __('My Profile') }}
                                    </a>
                                    <a class="dropdown-item" href="/admin/users/block">
                                        {{ __('Blocked Users') }}
                                    </a>
                                    <a class="dropdown-item" href="/admin/posts/block">
                                        {{ __('Blocked Posts') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('admin.password') }}">
                                        {{ __('Change Password') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('admin.logout') }}"
                                        onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('admin.logout') }}" method="GET" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @elseif(!Auth::guard('admin-web')->check() && !Auth::guard('web')->check())
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                                
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script>
    $(document).ready(function() {
        $(".searchBlog").click(function() {
            $(".searchForm").toggle("slide");
        });
    });
    </script>
</body>
</html>
