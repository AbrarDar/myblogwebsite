@extends('layouts.app')

@section('title', ' | View Post')

@section('content')

<div class="row">
    <div class="col-sm-7 offset-sm-1">
        <h1>{{ $post->title }}</h1>
        <p class="lead"> {{ $post->body }}</p>
    </div>

    <div class="col-sm-2 offset-sm-1">
        <div class="well">
            <dl class="dl-horizontal">
                <dt>URL:</dt>
                <dd><a href="{{ route('blog.single',$post->slug) }}">{{substr(url('blog/'.$post->slug), 0 ,20)}}{{ strlen(url($post->slug))>20 ? "..." : ""}}</a></dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Create At:</dt>
                <dd>{{date('M j, Y h:iA', strtotime($post->created_at))}}</dd>
            </dl>
            <dl class="dl-horizontal">
                <dt>Last Update At:</dt>
                <dd>{{date('M j, Y h:iA', strtotime($post->updated_at))}}</dd>
            </dl>
            <div class="row">
                <div class="col-sm-6">
                    {!! Form::open(['route'=>['admin.user.posts.update', $post->id], 'method' => 'PUT']) !!}
                    {!! Form::submit($post->block ? 'Unblock' : 'Block', ['class'=>$post->block ? 'btn btn-danger btn-block' : 'btn btn-warning btn-block']) !!}
                    {!! Form::close() !!}
                </div>
                <div class="col-sm-6">
                    {!! Form::open(['route'=>['admin.user.posts.destroy', $post->id], 'method' => 'DELETE']) !!}
                    {!! Form::submit('Delete', ['class'=>'btn btn-danger btn-block']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    {{ Html::linkRoute('admin.users.show', '<< See All Posts of User', $post->user_id, ['class' => 'btn btn-primary btn-block', 'style'=>'margin-top: 10px; align-items:center;'])}}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection