@extends('layouts.app')

@section('title', ' | All Post')

@section('content')

    @if (Session::has('blocked'))

        <div class="row">
            <div class="col-md-10 offset-md-1 alert alert-danger alert-dismissible fade show" role="alert">
                <p>{{ Session::get('blocked') }}</p>
            </div>
        </div>

    @elseif (Session::has('unblocked'))

        <div class="row">
            <div class="col-md-10 offset-md-1 alert alert-primary alert-dismissible fade show" role="alert">
                <p>{{ Session::get('unblocked') }}</p>
            </div>
        </div>

    @elseif (Session::has('success'))

        <div class="row">
            <div class="col-md-10 offset-md-1 alert alert-primary alert-dismissible fade show" role="alert">
                <p>{{ Session::get('success') }}</p>
            </div>
        </div>

    @endif

    <div class="row">
        <div class="col-md-8 offset-md-1">
            <h1>All User</h1>
        </div>
        <div class="col-md-2">
            <button class="searchBlog btn btn-lg btn-block btn-primary" >Search User</button>
        </div>
    </div>

    <div class="row" style="margin:10px;">
        <div class="col-md-8 offset-md-2">
            <div class="searchForm" style="display: none;">
                <div class="card">
                    <div class="card-header text-center"><h3>{{ __('Post Search') }}</h3></div>
    
                    <div class="card-body">
    
                        <form method="POST" action="{{ route('admin.users.search') }}" aria-label="{{ __('Register') }}">
                            @csrf
    
                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('Name') }}</label>
                                <div class="col-md-8">
                                    <input id="name" type="text"
                                        class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                                        value="{{ old('name') }}" >
    
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email"
                                    class="col-md-2 col-form-label text-md-right">{{ __('Email') }}</label>
                                <div class="col-md-8">
                                    <input id="email" type="email"
                                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                        value="{{ old('email') }}" >
    
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="city" class="col-md-2 col-form-label text-md-right">{{ __('City') }}</label>
                                <div class="col-md-8">
                                    <input id="city" type="text"
                                        class="form-control" name="city"
                                        value="{{ old('city') }}" >
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="country"
                                    class="col-md-2 col-form-label text-md-right">{{ __('Country') }}</label>
                                <div class="col-md-8">
                                    <input id="country" type="text"
                                        class="form-control"
                                        name="country" value="{{ old('country') }}" >
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="gender" class="col-md-2 col-form-label text-md-right">{{ __('Gender') }}</label>
                                <div class="col-md-8">
                                    <div class="form-control">
                                        <div class="form-check form-check-inline">
                                            <input type="radio" id="male" name="gender" class="form-check-input"
                                                value="male">
                                            <label for="male" class="form-check-label">Male</label><br>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input type="radio" id="female" name="gender" class="form-check-input"
                                                value="female">
                                            <label for="female" class="form-check-label">Female</label><br>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input type="radio" id="other" name="gender" class="form-check-input"
                                                value="other">
                                            <label for="other" class="form-check-label">Other</label>
                                        </div>
                                    </div>
    
                                    @if ($errors->has('gender'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('gender') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
    
                            <div class="form-group row mb-0">
                                <div class="col-md-2 offset-md-5">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        {{ __('Search') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="row">
        <div class="col-md-10 offset-md-1">
            <table class="table" style="margin-top: 20px;">
                <thead>
                    <th style="width: 20%">User Name</th>
                    <th style="width: 20%">Email</th>
                    <th style="width: 20%">Address</th>
                    <th style="width: 15%">No. of Post</th>
                    <th style="width: 10%">Gender</th>
                    <th style="width: 10%">Age</th>
                    <th style="width: 5%"></th>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->city.", ".$user->country }}</td>
                            <td>{{ $user->posts->count() }}</td>
                            <td>{{ $user->gender }}</td>
                            <td>{{ $user->age." years" }}</td>
                            
                            <td>
                                <div class="row">
                                    <a href="{{ route('admin.users.show', $user->id)}}" class="btn btn-primary btn-block">View</a>
                                    {!! Form::open(['route'=>['admin.users.update', $user->id], 'method' => 'PUT']) !!}
                                    {!! Form::submit($user->block ? 'Unblock' : 'Block', ['class'=>$user->block ? 'btn btn-danger btn-block' : 'btn btn-warning btn-block', 'style'=>'margin-top: 10px; width: 80px;']) !!}
                                    {!! Form::close() !!}
                                    {!! Form::open(['route'=>['admin.users.destroy', $user->id], 'method' => 'DELETE']) !!}
                                    {!! Form::submit('Delete', ['class'=>'btn btn-danger btn-block', 'style'=>'margin-top: 10px; width: 80px;']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            
            <hr>

            <div class="" style="display: flex; justify-content: center;">
                {!! $users->links(); !!}
            </div>
        </div>
    </div>
@endsection