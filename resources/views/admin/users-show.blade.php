@extends('layouts.app')

@section('title', ' | All Post')

@section('content')

    @if (Session::has('success'))
        <div class="row">
            <div class="col-md-10 offset-md-1 alert alert-danger alert-dismissible fade show" role="alert">
                <p>{{ Session::get('success') }}</p>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-header" style="text-align: center;">
                    <h4>USER DETAIL</h4>
                </div>
                
                <div class="card-body">
                    
                    <div class="row align-items-center">
                        <div class="col-sm-4 offset-sm-4" >
                            <img  src="{{URL::to("$user->image")}}" alt="User Image" style='width: 100px;height: 100px; border-radius: 50%; margin-left: 25px; margin-right: 10px;
                            object-fit: cover;'>
                        </div>
                    </div>

                    <div class="row" style="margin-top:20px;">
                        <label class="col-sm-3 offset-sm-3 ">Name</label>
                        <div class="col-sm-3">
                            <p>{{ $user->name }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-3 offset-sm-3 ">Email</label>
                        <div class="col-sm-3">
                            <p>{{ $user->email }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-3 offset-sm-3 ">Address</label>
                        <div class="col-sm-3">
                            <p>{{ $user->city.", ".$user->country }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-3 offset-sm-3 ">Gender</label>
                        <div class="col-sm-3">
                            <p>{{ $user->gender }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-3 offset-sm-3 ">Age</label>
                        <div class="col-sm-3">
                            <p>{{ $user->age." years" }}</p>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-3 offset-sm-3 ">Create At</label>
                        <div class="col-sm-3">
                            <p>{{ date('M j, Y ', strtotime($user->created_at)) }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top: 50px;">
        <div class="col-md-6 offset-md-1">
            <h1>All Post</h1>
        </div>
        <div class="col-md-2">
            {!! Form::open(['route'=>['admin.users.update', $user->id], 'method' => 'PUT']) !!}
            {!! Form::submit($user->block ? 'Unblock User' : 'Block User', ['class'=>$user->block ? 'btn btn-danger btn-block' : 'btn btn-warning btn-block', 'style'=>'height: 45px;']) !!}
            {!! Form::close() !!}
        </div>
        <div class="col-md-2">
            <a href="{{ route('posts.create') }}" class="btn btn-lg btn-block btn-primary">Search Post</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10 offset-md-1">
            <table class="table" style="margin-top: 20px;">
                <thead>
                    <th style="width: 35%">Title</th>
                    <th style="width: 45%">Body</th>
                    <th style="width: 15%">Create</th>
                    <th style="width: 5%"></th>
                </thead>
                <tbody>
                        @foreach($user->posts as $post)
                            <tr>
                                <td>{{ $post->title }}</td>
                                <td>{{ substr($post->body, 0, 250) }}{{ strlen($post->body)>250 ? "..." : ""}}</td>
                                <td>{{ date('M j, Y h:iA', strtotime($post->created_at)) }}</td>
                                <td>
                                    <div class="row">
                                        <a href="{{ route('admin.user.posts.show', $post->id)}}" class="btn btn-primary btn-block">View</a>
                                        {!! Form::open(['route'=>['admin.user.posts.update', $post->id], 'method' => 'PUT']) !!}
                                        {!! Form::submit($post->block ? 'Unblock' : 'Block', ['class'=>$post->block ? 'btn btn-danger btn-block' : 'btn btn-warning btn-block', 'style'=>'margin-top: 10px; width: 80px;']) !!}
                                        {!! Form::close() !!}
                                        {!! Form::open(['route'=>['admin.user.posts.destroy', $post->id], 'method' => 'DELETE']) !!}
                                        {!! Form::submit('Delete', ['class'=>'btn btn-danger btn-block', 'style'=>'margin-top: 10px; width: 80px;']) !!}
                                        {!! Form::close() !!}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                </tbody>
            </table>

            <hr>
            
        </div>
    </div>
@endsection